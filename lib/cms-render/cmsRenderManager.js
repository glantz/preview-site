var cache = require('nconf');

var CmsRender = function(req, res) {
  var cmsRender = require('./client.js');
  var clientConfig;
  if(process.env.NODE_ENV) {
    clientConfig = {
      host: cache.get('bucket_meta_dictionary').template_custom + '.cloudpen.io',
      secure: false
    }
  } else {
    clientConfig = {
      host: 'localhost',
      port: '5000',
      secure: false
    }
  }
  this.client = new cmsRender.Client(clientConfig);
  this.req = req;
  this.res = res;
};

CmsRender.prototype.updateFile = function(filePath, content, callback) {
  // console.log(cache.get('bucket_meta_dictionary').template_custom);
  this.client.post('/render/events/update-file', {file_path: filePath, content: content})
    .then(function(response) {
      callback(null, response);
    })
    .catch(function(err) {
      callback(err);
    });
};

exports.CmsRenderController = CmsRender;






// updateFile: function(fileName, callback) {
//   client.get('/apps')
//     .then(function(response) {
//       callback(null, response);
//     })
//     .catch(function(err) {
//       callback(err);
//     });
// }
