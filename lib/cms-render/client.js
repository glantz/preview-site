var merge = require('merge');
var request = require('superagent');
var axios = require('axios');
var querystring = require('qs');

/**
 * Client
 * Create a new instance of client and add the correct options
 * @param {object} options
 */
var Client = function(options) {
  this.defaults = {
    http: 'https',
    version: 'v1',
    bucket: null,
    secure : true,
    host : null,
    port : null,
    accessToken : null,
    clientId: null,
    clientSecret: null,
    code: null,
    username : null,
    password : null
  };
  this.options = merge.recursive(this.defaults, options);
  this.options.accessToken = this.options.accessToken;
  this.options.http = !this.options.secure ? 'http' : this.options.http; 
  this.options.uri = this.options.http + '://' + this.options.host + (this.options.port ? ':' + this.options.port : '');
};

/**
 * Management
*/
Client.prototype.requestManager = function(path, options, callback) {
  var fromRoot = false;
  path = 
    typeof path !== 'undefined' ?
      path.substring(0, 1) !== '/' ?
      '/' + path :
      path :
    '';
  options =
    options ?
    options :
    {};
  var uri;
  var qs =
    options.query ?
    '?' + querystring.stringify(options.query) :
    '';
  uri = [
    this.options.uri,
    path,
    qs
  ].join('');
  options.url = uri;
  options.data = 
    options.data ?
    options.data :
    {};
  options.method = 
    options.method ?
    options.method :
    'get';
  options.headers = 
    options.headers ?
    options.headers :
    {};
  options.headers['Content-Type'] = 'application/json;charset=utf-8';
  if (this.options.username && this.options.password) {
    options.headers['Authorization'] = 'Basic ' + new Buffer(this.options.username + ':' + this.options.password).toString('base64');
  } else if(!options.headers['Authorization']) {
    options.headers['Authorization'] = 'Bearer ' + this.options.accessToken;
  }
  var response = 
      axios(options)
      .then(function(response) {
        // if (!response.data) {
        //   if (callback) {
        //     callback(null, null);
        //   }
        //   return;
        // }
        // Fix this. There should always be a data object.
        if (callback) {
          if (response.data.data) {
            callback(null, response.data.data);
          } else {
            callback(null, response.data);
          }
        }
        if (response.data.data) {
          return response.data.data;
        } else {
          return response.data;
        }
      })
      .catch(function(error) {
        if (callback) {
          callback(error.data);
        }
        return error.data;
      });
  return response;
}

/**
 * POST
 * @param  {string}   path
 * @param  {object}   query
 * @return {object}            
 */
Client.prototype.post = function(path, data, callback) {
  var options = {
    method: 'POST',
    data: data
  }
  return this.requestManager(path, options, callback);
};

/**
 * GET
 * @param  {string}   path
 * @param  {object}   query
 * @return {object}            
 */
Client.prototype.get = function(path, query, callback) {
  var options = {
    method: 'GET',
    query: query
  }
  return this.requestManager(path, options, callback);
};

/**
 * PUT
 * @param  {string}   path
 * @param  {object}   query
 * @return {object}            
 */
Client.prototype.put = function(path, data, callback) {
  var options = {
    method: 'PUT',
    data: data
  }
  return this.requestManager(path, options, callback);
};

/**
 * DELETE
 * @param  {string}   path
 * @return {object}            
 */
Client.prototype.delete = function(path, callback) {
  var options = {
    method: 'DELETE'
  }
  return this.requestManager(path, options, callback);
};

exports.Client = Client;