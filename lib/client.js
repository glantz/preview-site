// route middleware to make sure that the client is initialized
module.exports = function(req, res, next) {
  var appStart = function(data) {
    req.apiClient = client;
    req.bucketId = data.bucket_id;
    req.accessToken = data.access_token;
    next();
  }
  var genericError = function(err) {
    console.log(err);
    next();
  }
  
  var cms;
  if(process.env.NODE_ENV) {
    cms = require('../bower_components/sdk/index.js');
    var clientConfig = {
      clientId: process.env.CLIENT_ID,
      clientSecret: process.env.CLIENT_SECRET,
      code: req.query.code
    }
  } else {
    cms = require('../../../sdk/index.js');
    var clientConfig = {
      host: 'localhost', 
      port: '8080',
      secure: false,
      clientId: process.env.CLIENT_ID,
      clientSecret: process.env.CLIENT_SECRET,
      code: req.query.code
    }
  }
  var client = new cms.Client(clientConfig);
  client.init()
    .then(appStart)
    .catch(genericError);
};