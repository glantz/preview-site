var github = require('../lib/github/githubManager');
var cache = require('nconf');       
var merge = require('merge');
var formidable = require('formidable');
var fs = require('fs');

TreeView = function(options, req, res) {
  var defaults = {}; 
  this.options = merge.recursive(defaults, options); 
  this.client = req.client; 
  this.req = req;  
  this.res = res;
  // console.log(cache.get('bucket_meta_dictionary'))
  this.repoName = cache.get('bucket_meta_dictionary')['template_custom'];
}; 

TreeView.prototype.handle = function() { 
  var action = this.req.body.action || this.req.query.action;  
  action = this.req.query.dir ? 'get_tree' : action;
  
  switch (action) {
    case 'get_tree': 
      this.getTree();
      break;
    case 'get_file_content':
      this.getFileContent(this.req.query.file_path); 
      break;
    case 'create_dir':
      this.createFile(this.req.body.file_path, this.req.body.file_content, true);
      break;
    case 'create_file':
      this.createFile(this.req.body.file_path, this.req.body.file_content);
      break;
    case 'create_media':
      this.createMedia(this.req, this.res);
      break;
    case 'change_file_name': 
      this.updateFileName(this.req.body.sha, this.req.body.file_path, this.req.body.file_path_old);
      break;
    case 'save_file':
      this.updateFile(this.req.body.sha, this.req.body.file_path, this.req.body.file_content);
      break; 
    case 'delete_file':
      this.deleteFile(this.req.body.sha, this.req.body.parent_sha, this.req.body.file_path, this.req.body.parent_file_path);
      break;
    default: 
      this.renderDefault();
  }
}; 
    
TreeView.prototype.getTree = function() {
  this.setCache(function(err, cachedTree) {
    if(err) {
      console.log(err);
      this.res.send(err);
    } else {
      // console.log(cachedTree);
      this.buildTree(cachedTree);
    }
  }.bind(this));
};   
  
TreeView.prototype.buildTree = function(tree) {
  var buildTree = [];
  var sha = this.req.query.dir !== '/' ?
            //this.req.query.dir.split('/')[this.req.query.dir.split('/').length-2] :
            this.req.query.dir.substring(1, this.req.query.dir.length) :
            null;
  // console.log(sha);
  for (var i = 0; i < tree.length; i++) {
    var item = tree[i];
    if (sha) {
      // console.log(item.path.substring(0, item.path.lastIndexOf('/')+1));
      // console.log(sha);
      if (item.path.substring(0, item.path.lastIndexOf('/')+1) !== sha) {
        continue;
      }
    } else {
      if (~item.path.indexOf('/')) {
        continue;
      } 
    }
    if(item.path === 'README.md') {
      // continue;
    }
    var type = item.type === 'blob' || item.type === 'file' ?
               'file' :
               'dir';
    var ext = type === 'file' ?
              item.path.split('.')[1] :
              '';
    var file = item.path.split('/')[item.path.split('/').length-1];
    var caption = item.path.split('/')[item.path.split('/').length-1];
    var treeNode = {
      type: type,
      dir: item.path,
      file: file,
      sha: item.sha,
      ext: ext,
      caption: caption,
      disabled: false,
      system: false,
      extension: false
    }
    buildTree.push(treeNode);
  }
  var sortedTree = [];
  var dirs = buildTree.filter(function (a) {
    return a.type === 'dir';
  });
  var files = buildTree.filter(function (a) {
    return a.type === 'file';
  });
  dirs = dirs.sort(function(a, b){
    var valA = a.file.toLowerCase();
    var valB = b.file.toLowerCase();
    if (valA < valB) //sort string ascending
      return -1
    if (valA > valB)
      return 1
    return 0 //default return value (no sorting)
  })
  files = files.sort(function(a, b){
    var valA = a.file.toLowerCase();
    var valB = b.file.toLowerCase();
    if (valA < valB) //sort string ascending
      return -1 
    if (valA > valB)
      return 1
    return 0 //default return value (no sorting)
  })
  sortedTree.push.apply(sortedTree, dirs);
  sortedTree.push.apply(sortedTree, files);
  this.res.json(sortedTree);
}; 
 
TreeView.prototype.getFileContent = function(filePath) {
  if (cache.get('tree')) {
    var tree = cache.get('tree');
    var sha;
    filePath = filePath.substring(1, filePath.length);
    for (var i = 0; i < tree.length; i++) {
      var treeNode = tree[i];
      treeNodePath = treeNode.path
      if (treeNodePath === filePath) {
        sha = treeNode.sha;
        break;
      }
    }
    // console.log('sha:' + sha);
    if (sha) {
      github.getBlob(sha, this.repoName, function(err, blob) {
        if(err) {
          this.res.send(err);
          console.log(err);
        } else {
          var content = new Buffer(blob.content, 'base64').toString('utf8');
          content = this.escapeHTML(content);
          // var data = {
          //   content: content
          // }
          this.res.send(content);
        }
      }.bind(this));  
    } else {
      this.res.json({message: 'No cache is set'});  
    }
  } else {
    this.res.json({message: 'No cache is set'});
  }
};     
   
TreeView.prototype.createFile = function(filePath, content, isDir) {
  github.createFile(filePath, this.repoName, 'Committing stuff1', '', function(err, data) {
    if(err) {
      this.res.json(err);
    } else { 
      if (!isDir) {
        console.log(data); 
        this.addItemToCache(data.content);
        this.res.json(data); 
      } else {
        cache.clear('tree');
        this.setCache(function(err, cachedTree) {
          this.res.json(data); 
        }.bind(this));  
      }
    }
  }.bind(this));
};

TreeView.prototype.createFileBinary = function(filePath, content) {
  github.createFile(filePath, this.repoName, 'Committing stuff1', content, function(err, data) {
    if(err) {
      this.res.json(err);
    } else { 
      console.log(data); 
      this.addItemToCache(data.content);
      this.res.json(data); 
    }
  }.bind(this));
};

TreeView.prototype.createMedia = function(req, res) {
  var self = this;
  var form = new formidable.IncomingForm();
  form.parse(req, function(err, fields, files) {
    var file = files.file;
    var filePath = fields.file_path;
    fs.readFile(file.path, function (err, content) {
      self.createFileBinary(filePath + file.name, content); 
    });
  });
};   
  
TreeView.prototype.updateFile = function(sha, filePath, content) {
  github.updateFile(sha, filePath, this.repoName, 'Committing stuff', content, function(err, data) {
    if(err) { 
      this.res.json(err);  
    } else {
      this.updateCache(sha, filePath, data.content.sha, data.content.path);
      this.res.json(data); 
      // cache.clear('tree');
      // this.setCache(function(err, cachedTree) { 
      //   this.res.json(data); 
      // }.bind(this));
    }
  }.bind(this));
  var CmsRenderController = require('../lib/cms-render/cmsRenderManager').CmsRenderController;
  var cmsRenderController = new CmsRenderController(this.req, this.res);
  cmsRenderController.updateFile(filePath, content, function(err, data) {
    if(err) {
      console.log(err);
    } else {
      console.log('response: ', data.message);
    }
  });
}; 
 
TreeView.prototype.updateFileName = function(sha, filePath, filePathOld) {
  // Get content for tree node
  github.getBlob(sha, this.repoName, function(err, blob) { 
    if(err) {
      this.res.json(err); 
    } else {  
      var content = new Buffer(blob.content, 'base64').toString('utf8');
      // Create new file
      github.createFile(filePath, this.repoName, 'Committing stuff', content, function(err, data) {
        if(err) { 
          this.res.json(err);
        } else {
          // Delete old file
          github.deleteFile(sha, filePathOld, this.repoName, 'Committing stuff', function(err, deletedFileData) {
            if(err) {
              this.res.json(err); 
            } else {
              this.updateCache(sha, filePathOld, data.content.sha, data.content.path);
              this.res.json(blob); 
            }
          }.bind(this));  
        }
      }.bind(this));
    }
  }.bind(this));
};
   
TreeView.prototype.deleteFile = function(sha, parentSha, filePath, parentFilePath) {
  github.deleteFile(sha, filePath, this.repoName, 'Committing stuff', function(err, data) {
    if(err) {  
      this.res.json(err);    
    } else {  
      // console.log(data.commit.parents);
      this.removeItemFromCache(sha, parentSha, filePath, parentFilePath);
      this.res.json(data);
      // cache.clear('tree');
      // this.setCache(function(err, cachedTree) {
      //   this.res.json(data); 
      // }.bind(this));
    }
  }.bind(this));
};

TreeView.prototype.setCache = function(callback) {
  if (cache.get('tree')) {
    callback(null, cache.get('tree'));
  } else {
    github.getTree(null, this.repoName, true, function(err, tree) {
      cache.set('tree', tree);
      callback(err, tree);
    }.bind(this));
  }
}; 

TreeView.prototype.getItemFromCache = function(sha) {
  var item;
  if (cache.get('tree')) {
    var tree = cache.get('tree');
    for (var i = 0; i < tree.length; i++) {
      var treeNode = tree[i];
      if (treeNode.sha === sha) {
        item = treeNode;
        break;
      }
    }
    return item;
  } else {
    this.res.json({message: 'No cache is set'});
    return null;
  }
};

TreeView.prototype.updateCache = function(oldSha, oldFilePath, newSha, newFilePath) {
  if (cache.get('tree')) {
    console.log(oldFilePath);
    var tree = cache.get('tree');
    for (var i = 0; i < tree.length; i++) {
      var treeNode = tree[i];
      if (treeNode.path === oldFilePath) {
        treeNode.sha = newSha;
        treeNode.path = newFilePath;
        console.log('Found: ' + newFilePath)
        break;
      }
    }
    cache.set('tree', tree);
  } else {
    this.res.json({message: 'No cache is set'});
  }
};

TreeView.prototype.removeItemFromCache = function(sha, parentSha, filePath, parentFilePath) {
  if (cache.get('tree')) { 
    var tree = cache.get('tree');
    var newTree = tree.filter(function(item) {
      // if (item.path === filePath) {
      //   console.log(item.path);
      // }
      // return (item.path !== filePath);
      // return (item.sha !== sha && item.path !== filePath);
      if (parentFilePath.length) {
        return (item.path !== filePath && item.path !== parentFilePath);
        // return (item.sha !== sha && item.sha !== parentSha);
      } else {
        return (item.path !== filePath);
        // return (item.sha !== sha);  
      }
    });
    cache.set('tree', newTree);
  } else {
    this.res.json({message: 'No cache is set'});
  }
};

TreeView.prototype.addItemToCache = function(item) {
  if (cache.get('tree')) {
    var tree = cache.get('tree');
    tree.push(item);
    cache.set('tree', tree);
  } else {
    this.res.json({message: 'No cache is set'});
  }
};

TreeView.prototype.renderDefault = function() { 
  var model = {
    context: this.req.context,
    req: this.req,
    process_env_public: process.env.NODE_ENV
  }
  if (this.req.params.id === 'frame') {
    this.res.render('frame', model);
  } else {
    this.res.render('index', model);  
  }
  // Help code
  // this.client.get('/items', {type: 'nav-menu-item'})
  // .then(function(items) {
  //   console.log(items[0].attributes.slug);
  //   var dir =
  //   [
  //     {
  //       type: 'dir',
  //       dir: 'ho-content/themes/clean//assets/',
  //       file: 'assets',
  //       ext: '',
  //       caption: 'assets',
  //       disabled: false,
  //       system: false,
  //       extension: false
  //     },
  //     {
  //       type: 'file',
  //       dir: 'ho-content/themes/clean//assets/',
  //       file: 'test.css',
  //       ext: 'css',
  //       caption: 'test.css',
  //       disabled: false,
  //       system: false,
  //       extension: false
  //     }
  //   ]
  //   this.res.json(dir);
  // });
};

TreeView.prototype.escapeHTML = function(str) {
  return str
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/\"/g, '&quot;')
    .replace(/\'/g, '&#39;'); // '&apos;' is not valid HTML 4
};

exports.TreeViewController = TreeView;