var router = require('express').Router();

var TreeViewController = require('../controllers/tree-view').TreeViewController;

router.route('/')
  .get(function(req, res) {new TreeViewController(null, req, res).handle()})
  .post(function(req, res) {new TreeViewController(null, req, res).handle()});

router.route('/:id')
  .get(function(req, res) {new TreeViewController(null, req, res).handle()})

module.exports = router;