// jQuery File Tree Plugin
//
// Version 1.0
//
// Base on the work of Cory S.N. LaViska  A Beautiful Site (http://abeautifulsite.net/)
// Dual-licensed under the GNU General Public License and the MIT License
// Icons from famfamfam silk icon set thanks to http://www.famfamfam.com/lab/icons/silk/
//
// Usage : $('#jao').jaofiletree(options);
//
// Author: Damien BarrÃ¨re
// Website: http://www.crac-design.com

(function( $ ) {
  var options =  {
    'root'            : '/',
    'script'          : 'connectors/jaoconnector.php',
    'showroot'        : 'root',
    'onclick'         : function(elem,type,file){},
    'ondoubleclick'   : function(elem,type,file){},
    'onclickcaption'  : function(elem,type,file){},
    'oncheck'         : function(elem,checked,type,file){},
    'usecheckboxes'   : true, //can be true files dirs or false
    'expandSpeed'     : 500,
    'collapseSpeed'   : 500,
    'expandEasing'    : null,
    'collapseEasing'  : null,
    'canselect'       : true
  };
  
  var methods = {
    init: function(obj) { 
      if($(this).length==0){
        return;
      }
      isInitialized = false;
      // inValidCharactersInFileName = ['/','\\','?','%','*',':','|','"','<','>'];
      inValidCharactersInFileName = ['\\','?','%','*',':','|','"','<','>'];
      $this = $(this);
      $.extend(options, obj);

      if(options.showroot != '') {
        checkboxes = '';
        if(options.usecheckboxes===true || options.usecheckboxes==='dirs'){
            checkboxes = '<input type="checkbox" />';
        }
        $this.html('<ul class="jaofiletree"><li class="drive directory system collapsed selected">'+checkboxes+'<a href="#" data-file="'+options.root+'" data-type="dir"><span class="arrow"></span><span class="icon drive"></span><span class="caption">'+options.showroot+'</span></a></li></ul>');
      }
      openfolder(options.root);
    },
    open : function(dir, filePath, func) {
      openfolder(dir, filePath, func);
    },
    close : function(dir, func){
      closedir(dir, func);
    },
    isdir : function(filePath){
      return isDir(filePath);
    },
    createfile : function(createFolder){
            createfile(createFolder);
        },
    changefile : function(func){
            changefile(func);
        },
    deletefile : function(func){
            deletefile(func);
        },
    setselected : function(dir) {
            setactive(dir);
        },
    seteventers : function(dir) {
            setevents();
        },
    getcurrentitem : function(dir) {
      return getCurrentItem(dir);
    },
    getchecked : function() {
      var list = new Array();            
      var ik = 0;
      $this.find('input:checked + a').each(function() {
          list[ik] = {
              type : $(this).attr('data-type'),
              file : $(this).attr('data-file')
          }                
          ik++;
      });
      return list;
    },
    getselected : function(){
      var list = new Array();            
      var ik = 0;
      $this.find('li.selected > a').each(function(){
          list[ik] = {
              type : $(this).attr('data-type'),
              file : $(this).attr('data-file'),
              sha : $(this).attr('data-sha')
          }                
          ik++;
      });
      return list;
    }
  };

  setactive = function(dir) {
    // console.log('setactive');
    $this.find('li').removeClass('selected');
    $this.find('a[data-file="'+dir+'"]').parent().addClass('selected');
    $this.find('a[data-file="'+dir+'"]').focus();
  }

  isDir = function(filePath) {
    var currentItem = getCurrentItem(filePath);
    return (currentItem.find('> a').data('type') == 'dir');
  }

  isInvalidFilename = function(fileName) {
    var isInvalidFilename = false;
    if(fileName.length == 0) {
      isInvalidFilename = true;
    }
    for(var i = 0;i<inValidCharactersInFileName.length;i++) {
      if(fileName.indexOf(inValidCharactersInFileName[i]) != -1)
      {
        isInvalidFilename = true;
        break;
      }
    }     
    return isInvalidFilename;
  }

  createfile = function(createFolder) {
    //console.log('createfile');
    var filePath = $('#filetree').jaofiletree('getselected')[0].file;
    filePath = filePath.substring(1, filePath.length);
    filePath = filePath.substring(0, filePath.length-1);
    // var sha = $('#filetree').jaofiletree('getselected')[0].sha;
    var isDirectory = isDir(filePath);
    var currentItem = getCurrentItem(filePath);
    var fileName = null;
    var fileObj = null;
    var inputField = null;
    var liClass = null;
    var dataType = null;
    var newExt = null;
    var counter = 0;
    var slashForDir = '';
    if(createFolder) {
      liClass = 'directory collapsed';
      newExt = '';
      dataType = 'dir';
      fileName = 'New folder';
    } else {
      liClass = 'file ext_txt';
      newExt = 'ext_txt';
      dataType = 'file';
      fileName = 'Untitled';
    }
    var tempFileName = fileName;
    var path = null;
    if(isDirectory) {
      path = filePath;
      path = path.length > 0 ? path + '/' : path;
    } else {
      path = filePath.substring(0, (filePath.length - filePath.split('/')[filePath.split('/').length-2].length-1));
      // path = (path.length > 0) ? path + '/' : path;
    }
    // alert(path);
    if(isDirectory) {
      while(currentItem.find('a[data-file="'+path+tempFileName+'/"]').length > 0) {
        counter++;
        tempFileName = fileName + '.' + counter;
      }
    } else {
      //alert(path+tempFileName);
      while(currentItem.parent().parent().find('a[data-file="'+path+tempFileName+'/"]').length > 0) {
        counter++;
        tempFileName = fileName + '.' + counter;
      }
    }
    if(counter > 0) {
      fileName = fileName + '.' + counter
    }
    fileObj = $('<li class="'+liClass+'"><input type="checkbox" data-type="'+dataType+'" data-file="/'+fileName+'" /><a data-type="'+dataType+'" data-file="/'+fileName+'/"><span class="arrow"></span><span style="margin-top: -4px;" class="icon ext_'+newExt+'"></span><span class="caption">'+fileName+'</span></a></li>');
    if(isDirectory) {
      inputField = $('<input class="crud-input" type="text" value="'+fileName+'" id="input-create-folder" />');
      currentItem.find('> ul').prepend(fileObj);
    } else {
      inputField = $('<input class="crud-input" type="text" value="'+fileName+'" id="input-create-file" />');
      $(fileObj).insertAfter(currentItem);
    }
    inputField.insertBefore(fileObj.find('.caption'));
    fileObj.find('a .caption').css('display', 'none');
    //$this.jaofiletree('seteventers');
    $this.jaofiletree('setselected', '/'+fileName+'/');
    inputField.select();
    var create = function(fileName) {
      fileObj.addClass('wait');
      if(isInvalidFilename(fileName)) {
        fileObj.remove();
        //fileObj.find('a').css('display', 'block');
        //inputField.remove();
        alert('Is invalid filename');
        return;
      }
      var action = null;
      if(createFolder) {
        action = 'create_dir';
        slashForDir = '/Untitled';
      } else {
        action = 'create_file';
      }
      var data =
      'action=' + action +
      '&' +
      'file_path=' + path + fileName + slashForDir;
      $.ajax({
        type: 'POST',
        data: data,
        success: function(response) {
          if (response.content) {
            console.log(response.content.sha);
          }
          // console.log(response);
          // fileObj.find('a .caption').css('display', 'block');
          // inputField.remove();
          var parentFilePath = fileObj.parent().parent().find('> a').data('file');
          if(createFolder) {
            newFolder = fileObj.find('> a').data('file').replace('New folder', fileName)
            $this.jaofiletree('close', parentFilePath);
            $this.jaofiletree('open', parentFilePath, newFolder, function() {
              $this.jaofiletree('open', newFolder, newFolder + 'Untitled' + '/');
              // console.log('Set active:' + newFolder + 'Untitled' + '/');
            });
            //alert(fileObj.find('> a').data('file'));
            //$this.jaofiletree('close', '/' + fileName + '/');
            //$this.jaofiletree('open', fileObj.find('> a').data('file'));
          } else {
            $this.jaofiletree('close', parentFilePath);
            $this.jaofiletree('open', parentFilePath, fileObj.find('> a').data('file').replace('Untitled', fileName));
          }
          // $('#filetree').jaofiletree('setselected', fileObj.find('> a').data('file'));
          // $('#filetree').jaofiletree('setselected', '/');
        }
      });
    }
    inputField.keydown(function(e) {
      if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        fileObj.unbind();
        create(this.value);
        e.preventDefault();
        return false;
      }
    }); 
    fileObj.find('.crud-input').blur(function(e) {
      create(this.value);
    });
  }

  changefile = function(func) {
    var filePath = $('#filetree').jaofiletree('getselected')[0].file;
    // filePath = filePath.substring(1, filePath.length);
    // filePath = filePath.substring(0, filePath.length-1);
    var sha = $('#filetree').jaofiletree('getselected')[0].sha;
    // alert(filePath); 
    // alert(sha);
    var isDirectory = isDir(filePath);
    if(isDirectory) {
      alert('Det går inte att ändra mappnamn i denna version av Cloudpen.');
      return;
    }
    var currentItem = getCurrentItem(filePath);
    var currentFileName = filePath.split('/')[filePath.split('/').length-2];
    var inputField = $('<input class="crud-input" type="text" value="'+currentFileName+'" id="input-change-file" />');
    inputField.insertBefore(currentItem.find('> a > .caption'));
    currentItem.find('> a .caption').css('display', 'none');
    currentItem.find('> a .icon').css('margin-top', '-3px');
    inputField.select();
    currentItem.find('> a').removeAttr('href');
    //currentItem.unbind();
    //currentItem.find('a').unbind();
    //inputField.unbind();
    //currentItem.find('.caption').unbind();
    var change = function(fileName) {
      currentItem.addClass('wait');
      if(isInvalidFilename(fileName)) {
        alert('Is invalid filename');
        currentItem.find('a').css('display', 'block');
        inputField.remove();
        return;
      }
      if(isDirectory) {
        action = 'change_folder_name';
      } else {
        action = 'change_file_name';
      }
      var path = filePath.substring(0, (filePath.length - filePath.split('/')[filePath.split('/').length-2].length-1));
      path = path.substring(1, path.length);
      var data =
      'action=' + action +
      '&' +
      'file_path=' + path + fileName +
      '&' +
      'sha=' + sha +
      '&' +
      'file_path_old=' + path + currentFileName;
      $.ajax({
        type: 'POST',
        data: data,
        success: function(response) {
          // console.log(response)
          currentItem.find('a').css('display', 'block');
          inputField.remove();
          // alert(currentItem.find('> a').data('file'));
          var parentFilePath = currentItem.parent().parent().find('> a').data('file');
          $this.jaofiletree('close', parentFilePath);
          $this.jaofiletree('open', parentFilePath, currentItem.find('> a').data('file').replace(currentFileName, fileName), function() {
            // $this.jaofiletree('setselected', currentItem.find('> a').data('file').replace(currentFileName, fileName));
            func(currentItem.find('> a').data('file'), currentItem.find('> a').data('file').replace(currentFileName, fileName), response.sha);
          });
        }
      });
    }
    inputField.keydown(function(e) {
      if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        currentItem.unbind();
        //currentItem.find('input').unbind();
        change(this.value);
        e.preventDefault();
        return false;
      }
    });
    currentItem.find('.crud-input').blur(function(e) {
      change(this.value);
    });
  }

  deletefile = function(func) {
    var filePath = $this.jaofiletree('getselected')[0].file;
    var currentItem = $this.jaofiletree('getcurrentitem', filePath);
    var isDirectory = isDir(filePath);
    if (isDirectory) {
      alert('Det går inte att ta bort mappar direkt i den här versionen.\nDu behöver ta bort varje fil för sig i mappen.');
      return;
    }
    filePath = filePath.substring(1, filePath.length);
    filePath = filePath.substring(0, filePath.length-1);
    var sha = $this.jaofiletree('getselected')[0].sha;
    var parentSha = currentItem.parent().parent().find('> a').data('sha');
    var parentFilePath = currentItem.parent().parent().find('> a').data('file');
    parentFilePath = parentFilePath.substring(1, parentFilePath.length);
    parentFilePath = parentFilePath.substring(0, parentFilePath.length-1);
    // alert(currentItem.parent().parent().find('ul li').length);
    var itemsInDir = currentItem.parent().parent().find('ul li').length;
    if(itemsInDir > 1) {
      parentSha = '';
      parentFilePath = '';
    }
    // console.log('filePath: ' + filePath)
    // console.log('sha: ' + sha)
    //var deletePost = confirm('Ã„r du sÃ¤ker pÃ¥ att du vill ta bort filen ' + filePath +'?');
    var deletePost = true;
    var newSelectedElement = null;
    if(currentItem.next().length) {
      newSelectedElement = currentItem.next().find('> a').data('file');
    } else if(currentItem.prev().length) {
      newSelectedElement = currentItem.prev().find('> a').data('file');
    } else if(itemsInDir === 1) {
      newSelectedElement = currentItem.parent().parent().parent().parent().find('> a').data('file');
      // newSelectedElement = currentItem.parent().parent().find('> a').data('file');
    } else {
      newSelectedElement = currentItem.parent().parent().find('> a').data('file');
    }
    if(deletePost) {
      var currentItem = getCurrentItem(filePath);
      currentItem.addClass('wait');
      if(currentItem.find('> a').data('type') == 'dir') {
        //Delete folder
        var data =
        'action=delete_folder' +
        '&' +
        'sha=' + sha +
        '&' +
        'parent_sha=' + parentSha +
        '&' +
        'parent_file_path=' + parentFilePath +
        '&' +
        'file_path=' + filePath; 
        $.ajax({
          type: 'POST',
          data: data,
          success: function(response) {
            var parentFilePath = currentItem.parent().parent().find('> a').data('file');
            currentItem.remove();
            $this.jaofiletree('setselected', newSelectedElement);
            //$this.jaofiletree('close', parentFilePath, newSelectedElement);
            //$this.jaofiletree('open', parentFilePath, newSelectedElement);
            func();
          }
        });
      } else {
        //Delete file
        var data =
        'action=delete_file' +
        '&' +
        'sha=' + sha +
        '&' +
        'parent_sha=' + parentSha +
        '&' +
        'parent_file_path=' + parentFilePath +
        '&' +
        'file_path=' + filePath;
        $.ajax({
          type: 'POST',
          data: data,
          success: function(response) {
            // console.log(response)
            var parentFilePath = currentItem.parent().parent().find('> a').data('file');
            if (itemsInDir === 1) {
              parentFilePath = currentItem.parent().parent().parent().parent().find('> a').data('file');
              $this.jaofiletree('close', newSelectedElement);
              $this.jaofiletree('open', newSelectedElement, parentFilePath);
              // currentItem.remove();
              // $this.jaofiletree('setselected', newSelectedElement);  
            } else {
              currentItem.remove();
              $this.jaofiletree('setselected', newSelectedElement);  
            }
            //$this.jaofiletree('close', parentFilePath, newSelectedElement);
            //$this.jaofiletree('open', parentFilePath, newSelectedElement);
            func();
          }
        });
      }
    }
  }

  var cleanfilepath = function(filePath) {
    filePath = filePath.substring(0, filePath.length-1);
    return filePath;
  }

  getCurrentItem = function(filePath) {
    var filePathi = '/';
    if($('#filetree').jaofiletree('getselected').length) {
      //alert($('#filetree').jaofiletree('getselected').length);
      filePathi = $('#filetree').jaofiletree('getselected')[0].file;
    }
    var currentItem = $this.find('a[data-file="'+filePathi+'"]').parent();
    return currentItem;
  }

  openfolder = function(dir, filePath, func) {
    // console.log('dir: ' + dir);
    // console.log('filePath: ' + filePath);
    if($this.find('a[data-file="'+dir+'"]').parent().hasClass('expanded')) {
      return;
      }
    //console.log('openfolder');
    var ret;
    ret = $.ajax({
      url : options.script,
      data : {dir : dir},
      context : $this,
      dataType: 'json',
      beforeSend : function(){this.find('a[data-file="'+dir+'"]').parent().addClass('wait');}
    }).done(function(datas) {
      ret = '<ul class="jaofiletree" style="display: none">';
      for(ij=0; ij<datas.length; ij++) {
        var classe = null;
        if(datas[ij].type=='dir'){
          classe = 'directory collapsed';
        } else {
          classe = 'file ext_'+datas[ij].ext;
        }
        if(datas[ij].disabled) {
          classe += ' disabled';
        }
        if(datas[ij].system) {
          classe += ' system';
        }
        if(datas[ij].extension) {
          classe += ' extension';
        }
        ret += '<li class="'+classe+'">'                    
        if(options.usecheckboxes===true || (options.usecheckboxes==='dirs' && datas[ij].type=='dir') || (options.usecheckboxes==='files' && datas[ij].type=='file')){
          ret += '<input type="checkbox" data-file="'+dir+datas[ij].file+'" data-type="'+datas[ij].type+'"/>';
        }
        else{
          ret += '<input disabled="disabled" type="checkbox" data-file="'+dir+datas[ij].file+'" data-type="'+datas[ij].type+'"/>';
        }
        ret += '<a href="#" data-file="'+dir+datas[ij].file+'/" data-type="'+datas[ij].type+'" data-sha="'+datas[ij].sha+'">';
        ret += '<span class="arrow"></span>';
        ret += '<span class="icon ext_'+datas[ij].ext+'"></span>';
        ret += '<span class="caption">'+datas[ij].caption+'</span>';
        ret += '</a>';
        ret += '</li>';
      }
      ret += '</ul>';
            this.find('a[data-file="'+dir+'"]').parent().removeClass('wait').removeClass('collapsed').addClass('expanded');
      this.find('a[data-file="'+dir+'"]').after(ret);
      this.find('a[data-file="'+dir+'"]').next().slideDown(options.expandSpeed,options.expandEasing);
      if(options.usecheckboxes){
        this.find('li input[type="checkbox"]').attr('checked',null);
        this.find('a[data-file="'+dir+'"]').prev(':not(:disabled)').attr('checked','checked');
        this.find('a[data-file="'+dir+'"] + ul li input[type="checkbox"]:not(:disabled)').attr('checked','checked');
      }
      setevents();
    }).done(function(){
      //Trigger custom event
      $this.trigger('afteropen');
      $this.trigger('afterupdate');
      if(func) {
        func();
      }
      if(filePath) {
        $('#filetree').jaofiletree('setselected', filePath);
      }
      if(!isInitialized) {
        $this.trigger('initialized');
        $this.find('a[data-file="'+dir+'"]').focus();
        isInitialized = true;
      }
    });
    }

    closedir = function(dir, func) {
      //console.log('closedir');
      $this.find('a[data-file="'+dir+'"]').next().slideUp(options.collapseSpeed,options.collapseEasing,function(){
        $(this).remove();
        $this.find('a[data-file="'+dir+'"]').parent().removeClass('expanded').addClass('collapsed');
        setevents();
        //Trigger custom event
        $this.trigger('afterclose');
        $this.trigger('afterupdate');
        if (func) {
          func();
        }
      });
    }

    setevents = function(){
    //console.log('setevents');
    $this.find('li a .icon, li a .caption').unbind('dblclick');
    $this.find('li a').unbind('click');
        //$this.find('li a .caption').unbind('click');
    //Bind userdefined function on click an element
        //$this.find('li a .caption').mousedown(function(event){event.preventDefault();return false;});
    $this.find('li a .icon, li a .caption').bind('dblclick', function(event) {
          options.ondoubleclick(this, $(this).parent().attr('data-type'),$(this).parent().attr('data-file'));
      if(options.usecheckboxes && $(this).attr('data-type')=='file') {
        $this.find('li input[type="checkbox"]').attr('checked',null);
        $(this).prev(':not(:disabled)').attr('checked','checked');
        $(this).prev(':not(:disabled)').trigger('check');
      }
      return false;
    });
    $this.find('li a').bind('click', function(event) {
          //console.log($(event.target).attr('class') != null);
      $this.find('a').parent().removeClass('inactive');
      if($(event.target).attr('class') != null && ($(event.target).attr('class').indexOf('caption') != -1 || $(event.target).attr('class').indexOf('icon') != -1 || $(event.target).attr('class').indexOf('crud-input') != -1)) {
        if($(event.target).attr('class').indexOf('crud-input') == -1) {
          options.onclickcaption(this, $(this).attr('data-type'),$(this).attr('data-file'));
          if(options.canselect){
            $this.find('li').removeClass('selected');
            $(this).parent().addClass('selected');
          }
        } else {
          //console.log('crud-input');
          //return true;
        }
        //console.log('Prevent click on caption element');
        //event.preventDefault();
      } else {
        options.onclick(this, $(this).attr('data-type'),$(this).attr('data-file'));
        if(options.usecheckboxes && $(this).attr('data-type')=='file') {
            $this.find('li input[type="checkbox"]').attr('checked',null);
            $(this).prev(':not(:disabled)').attr('checked','checked');
            $(this).prev(':not(:disabled)').trigger('check');
        }
        //if(options.canselect){
        //  $this.find('li').removeClass('selected');
        //  $(this).parent().addClass('selected');
        //}
      }
            return false;
        });
        //Bind checkbox check/uncheck
        $this.find('li input[type="checkbox"]').bind('change', function() {
            options.oncheck(this,$(this).is(':checked'), $(this).next().attr('data-type'),$(this).next().attr('data-file'));
            if($(this).is(':checked')){
                $this.trigger('check');
            }else{
                $this.trigger('uncheck');
            }
        });
        //Bind for collapse or expand elements
        $this.find('li.directory.collapsed a').bind('click', function(event) {if($(event.target).attr('class') != null && ($(event.target).attr('class').indexOf('caption') != -1 || $(event.target).attr('class').indexOf('icon') != -1 || $(event.target).attr('class').indexOf('crud-input') != -1)) {return false;};methods.open($(this).attr('data-file'));return false;});
        $this.find('li.directory.expanded a').bind('click', function(event) {if($(event.target).attr('class') != null && ($(event.target).attr('class').indexOf('caption') != -1 || $(event.target).attr('class').indexOf('icon') != -1 || $(event.target).attr('class').indexOf('crud-input') != -1)) {return false;};methods.close($(this).attr('data-file'));return false;});        
    $this.find('li.directory.collapsed a .caption').bind('dblclick', function(event) {methods.open($(this).parent().attr('data-file'));return false;});
        $this.find('li.directory.expanded a .caption').bind('dblclick', function(event) {methods.close($(this).parent().attr('data-file'));return false;});        
    //$('#filetree li').off('contextmenu');
    //$('#filetree li:not(.disabled):not(.system):not(.extension)').on('contextmenu', function(e) { 
    $('#filetree li:not(.disabled)').on('contextmenu', function(e) { 
      //console.log('Context Menu event has fired!');
      //console.log($(this).find(' > a').data('file'));
      $this.jaofiletree('setselected', $(this).find(' > a').data('file'));
    });
    //$('#filetree li.disabled > a, #filetree li.system > a, #filetree li.extension > a').off('contextmenu');
    //$('#filetree li.disabled > a, #filetree li.system > a, #filetree li.extension > a').on('contextmenu', function(e) {
    //  $('#myModal-1').modal();
    //  $('#myModal-1 .modal-body').html('Denna katalog Ã¤r en del av systemets arkitektur och gÃ¥r ej att ta bort');
    //  $('#js-modal-1-ok').focus();
    //  $('#myModal-1').on('hidden.bs.modal', function (e) {
    //    $('#sidebar').click();
    //  })
    //  e.preventDefault();
    //  return false;
    //});
    $('#filetree li.disabled > a, #filetree li.system > a, #filetree li.extension > a').off('keypress');
    $('#filetree li.disabled > a, #filetree li.system > a, #filetree li.extension > a').on('keypress', function(e) {
      if(e.key === 'Del') {
        $('#myModal-1').modal();
        $('#myModal-1 .modal-body').html('Denna katalog Ã¤r en del av systemets arkitektur och gÃ¥r ej att ta bort');
        $('#js-modal-1-ok').focus();
        $('#myModal-1').on('hidden.bs.modal', function (e) {
          $('#sidebar').click();
          //var currentItem = $this.jaofiletree('getcurrentitem');
          //currentItem.find('> a').focus();
        })
        e.preventDefault();
        return false;
      }
    });
    ContextMenu.Init('#filetree li:not(.disabled)');
  }

    $.fn.jaofiletree = function( method ) {
        // Method calling logic
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            //error
        }    
  };
})(jQuery);