var ideCurrentDirectory = null;
var ideCurrentSelected = null;
function ideUploadFilesCallback() {
  $system('#filetree').jaofiletree('close', ideCurrentDirectory, ideCurrentSelected);
  $system('#filetree').jaofiletree('open', ideCurrentDirectory, ideCurrentSelected);
  closeDialogWindow('iframeUploadFiles');
}
//var clearTimeoutPollSassError;
var editorIsSaving = false;
(function($) {
  var $ = $system;
  $system.fn.ide = function(method) {
    return this.each(function() {
      //****************************************************************
      //Variables
      //****************************************************************
      var connector = '';
      var root = $('#filetree').data('root-name');
      var presettings = null;
      page = parent.frames['page'];
      pageAdmin = null;
      var page = null;
      if(window.top.HemsidaOnlineEdit) {
        page = window.top.parent.frames['page'];
        pageAdmin = window.top.parent.frames['page'];
      } else {
        page = parent.frames['page'];
        pageAdmin = window.top;
      }
      var getParameterByName = function(name) {
        var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
      }
      var mode = getParameterByName('editor_mode');
      var cookieName = 'editor_'+ mode;
      if($.cookie(cookieName))
      {
        presettings = JSON.parse($.cookie(cookieName));
      }
      var isRunning = false;
      //Events
      //****************************************************************
      $(window).resize(function() {
        setFullHeight();
      });
      $('#upload-files').click(function() {
        var imageFilters = [{title : "Ladda upp filer", extensions : "jpg,gif,png,js,css,liquid,less,eot,svg,ttf,woff,scss"}];
        var directory = mode.replace('s', '');
        var virtualPath = null;
        var filePath = $('#filetree').jaofiletree('getselected')[0].file;
        var currentSelected = $('#filetree').jaofiletree('getselected')[0].file;
        var isDirectory = $('#filetree').jaofiletree('isdir', filePath);
        if(!isDirectory) {
          filePath = filePath.substring(0, (filePath.length - filePath.split('/')[filePath.split('/').length-2].length-1));
        }
        virtualPath = '/'+root+filePath;
        ideCurrentDirectory = filePath;
        ideCurrentSelected = currentSelected;
        HemsidaOnline.UI.Upload.Files1(virtualPath, 'ideUploadFilesCallback', {iframe: '', filters: imageFilters}, directory);
      });
      $('#change-file').click(function() {
        $('#filetree').jaofiletree('changefile', function(oldFilePath, newFilePath) {
          updatePreSettingsTabs(oldFilePath, newFilePath);
        });
      });
      $('#js-modal-delete-file').click(function() {
        $('#delete-file').click();
      });
      $('#delete-file').click(function() {
        var filePath = $('#filetree').jaofiletree('getselected')[0].file;
        var currentItem = $('#filetree').jaofiletree('getcurrentitem', filePath);
        var isDirectory = $('#filetree').jaofiletree('isdir', filePath);
        var typeName = 'filen';
        if(isDirectory) {
          typeName = 'katalogen';
        }
        if($('#myModal').css('display') != 'block') {
          $('#myModal').modal();
          $('#myModal .modal-body').html('Är du säker på att du vill ta bort '+typeName+' "' + cleanFilePath(filePath).split('/')[cleanFilePath(filePath).split('/').length-1] + '"?');
          $('#js-modal-delete-file').focus(); 
          $('#myModal').on('shown.bs.modal', function (e) {
            $('#js-modal-delete-file').focus(); 
          })
          $('#myModal').on('hidden.bs.modal', function (e) {
            currentItem.find('> a').focus();
          })
          return;
        }
        $('#myModal').modal('hide');
        var newSelectedElement = null;
        if(currentItem.next().length) {
          newSelectedElement = currentItem.next().find('> a').data('file');
        } else if(currentItem.prev().length) {
          newSelectedElement = currentItem.prev().find('> a').data('file');
        } else {
          newSelectedElement = currentItem.parent().parent().find('> a').data('file');
        }
        $('#filetree').jaofiletree('deletefile', function() {
          removePreSettingsTabs(cleanFilePath(filePath));
          removePreSettingsExpanded(filePath);
          removeTab(cleanFilePath(filePath));
          setSelectedPreSettingsTree(newSelectedElement);
        });
      });
      $('#create-file, #create-folder').click(function() {
        var createFolder = $(this).attr('id') == 'create-folder';
        var filePath = $('#filetree').jaofiletree('getselected')[0].file;
        var isDirectory = $('#filetree').jaofiletree('isdir', filePath);
        var currentItem = $('#filetree').jaofiletree('getcurrentitem', filePath);
        if(isDirectory && currentItem.attr('class').indexOf('expanded') == -1) {
          $('#filetree').jaofiletree('open', filePath, null, function() {
            $('#filetree').jaofiletree('createfile', createFolder);
          });
        } else {
          $('#filetree').jaofiletree('createfile', createFolder);
        }
      });
      $('#main').on('click', function() {
        var currentItem = $('#filetree').jaofiletree('getcurrentitem');
        currentItem.addClass('inactive');
        //console.log('blurred');
      });
      $('#sidebar').on('click', function(event) {
        if($(event.target).attr('class') != null && $(event.target).attr('class').indexOf('crud-input') != -1) {
          //console.log($(event.target).attr('class')); 
        } else {
          var currentItem = $('#filetree').jaofiletree('getcurrentitem');
          currentItem.removeClass('inactive');
          currentItem.find('> a').focus();
          //console.log('unblurred');
        }
      });
      //$('#filetree').delegate('li.selected a .icon, li.selected a .caption', 'mouseleave', function() {
      //  var filePath = $('#filetree').jaofiletree('getselected')[0].file;
      //  //console.log(filePath);
      //  setSelectedPreSettingsTree(filePath);
      //});
      $('#filetree').keypress(function(event) {
        if($('#filetree').find('input[id*="input-"]').length) {
          return;
        }
        var currentItem = $('#filetree').jaofiletree('getcurrentitem');
        var filePath = $('#filetree').jaofiletree('getselected')[0].file;
        //alert(event.key);
        if(event.key === 'Del') {
          $('#delete-file').click();
        }
        if(event.key === 'Down') {
          if(currentItem.find('> ul > li').length) {
            $('#filetree').jaofiletree('setselected', currentItem.find('> ul > li > a').data('file'));
            setSelectedPreSettingsTree(currentItem.find('> ul > li > a').data('file'));
          } else if(currentItem.next().length) {
            $('#filetree').jaofiletree('setselected', currentItem.next().find('> a').data('file'));
            setSelectedPreSettingsTree(currentItem.next().find('> a').data('file'));
          } else if(currentItem.parent().parent().next().length && currentItem.parent().parent().next().find('> a').length) {
            $('#filetree').jaofiletree('setselected', currentItem.parent().parent().next().find('> a').data('file'));
            setSelectedPreSettingsTree(currentItem.parent().parent().next().find('> a').data('file'));
          }
        }
        if(event.key === 'Up') {
          if(currentItem.prev().length) {
            $('#filetree').jaofiletree('setselected', currentItem.prev().find('> a').data('file'));
            setSelectedPreSettingsTree(currentItem.prev().find('> a').data('file'));
          } else if(currentItem.parent().parent().find('> a').length) {
            $('#filetree').jaofiletree('setselected', currentItem.parent().parent().find('> a').data('file'));
            setSelectedPreSettingsTree(currentItem.parent().parent().find('> a').data('file'));
          }
        }
        if(event.key === 'Left') {
          if(currentItem.find('> a').data('type') == 'dir' && currentItem.attr('class').indexOf('expanded') != -1) {
            $('#filetree').jaofiletree('close', filePath);
            removePreSettingsExpanded(filePath);
          } else if(currentItem.parent().attr('class').indexOf('jaofiletree') != -1 && currentItem.attr('class').indexOf('drive') == -1) {
            $('#filetree').jaofiletree('setselected', currentItem.parent().parent().find('> a').data('file'));
            setSelectedPreSettingsTree(currentItem.parent().parent().find('> a').data('file'));
            //removePreSettingsExpanded(filePath);
          }
        }
        if(event.key === 'Right') {
          if(currentItem.find('> a').data('type') == 'dir' && currentItem.attr('class').indexOf('expanded') == -1) {
            $('#filetree').jaofiletree('open', filePath);
            addPreSettingsExpanded(filePath);
          }
        }
        //if(event.key === 'Menu') {
        //  currentItem.contextmenu();
        //}
//        if(event.key === 'Enter') {
//          var filePath = $('#filetree').jaofiletree('getselected')[0].file;
//          var type = $('#filetree').jaofiletree('getselected')[0].type;
//          if(type != 'dir')
//          {
//            filePath = cleanFilePath(filePath);
//            var fileName = filePath;
//            if(fileName.indexOf('/') != -1)
//            {
//              fileName = fileName.split('/')[fileName.split('/').length-1];
//            }
//            createEditorInstance(fileName, filePath);
//          }
//          else
//          {
//            $('#filetree').jaofiletree('open', filePath);
//            addPreSettingsExpanded(filePath);
//          }
//        }
      });
        //xTriggered++;
        //var msg = "Handler for .keyup() called " + xTriggered + " time(s).";
        //console.log( msg, "html" );
        //console.log( event );
        //.keydown(function( event ) {
        //if ( event.which == 13 ) {
        //  event.preventDefault();
        //}
      //});
      //$( "#other").click(function() {
      //  $( "#target" ).keyup();
      //});
      //$('#create-folder').click(function() {
      //  $('#filetree').jaofiletree('createfolder');
      //});
      //****************************************************************
      //Functions
      //****************************************************************
      //Ide
      var getCurrentItem = function(filePath) {
        var fileTree = $('#filetree');
        var currentItem = fileTree.find('a[data-file="'+filePath+'"]').parent();
        return currentItem;
      }
      var tabExists = function(filePath) {
        var tabExists = false;
        var presettings = getPreSettings();
        for(var i = 0;i<presettings.tabs.length;i++)
        {
          if(presettings.tabs[i].filePath == filePath)
          {
            tabExists = true;
            break;
          }     
        }
        //tabExists = tabs.find('li[data-file="'+filePath+'"]').length;
        return tabExists;
      }
      var tabIsRendered = function(filePath) {
        var tabExists = false;
        var tabs = $('#tabs');
        tabExists = tabs.find('li[data-file="'+filePath+'"]').length;
        return tabExists;
      }
      var activateTab = function(filePath, isPreSetting) {
        if(tabExists(filePath))
        {
          var tabs = $('#tabs');
          var index = tabs.find('li[data-file="'+filePath+'"]').index();
          var editorContainer = $('#tabs-container');
          editorContainer.tabs('option', 'active', index);
          if(!isPreSetting) { 
            setSelectedPreSettingsTabs(index);
          }
        }
      }
      var setLoader = function(filePath) {
        var fileTree = $('#filetree');
        var loader = $('<div class="loader"></div>');
        fileTree.find('a[data-file="'+filePath+'/"]').parent().addClass('wait');
      }
      var removeLoader = function() {
        var fileTree = $('#filetree');
        fileTree.find('li').removeClass('wait');
      }
      var updateEnvironment = function() {
        //console.log('updateEnvironment');
      }
      var setFullHeight = function(editor) {
        var browserHeight = document.documentElement.clientHeight;
        var tabMenu = $('#tabs')[0].offsetHeight;
        $('.editor').css('height', (browserHeight - tabMenu) + 'px');
      }
      var addEditor = function(name, fileName) {
        var mode = 'xml';
        var cssFile = fileName.indexOf('.css') != -1 && fileName.indexOf('.css.liquid') == -1;
        var textFile = fileName.indexOf('.txt') != -1 || fileName.indexOf('.') == -1;
        var liquidFile = fileName.indexOf('.liquid') != -1;
        var lessFile = fileName.indexOf('.less') != -1;
        var sassFile = fileName.indexOf('.scss') != -1 || fileName.indexOf('.sass') != -1;
        var jsonFile = fileName.indexOf('.json') != -1;
        var jsFile = fileName.indexOf('.js') != -1 && fileName.indexOf('.js.liquid') == -1;
        if(liquidFile) {mode = 'htmlmixed';}
        if(jsonFile) {mode = 'css';}
        if(cssFile) {mode = 'css';}
        if(lessFile) {mode = 'less';}
        if(sassFile) {mode = 'sass';}
        if(jsFile) {mode = 'javascript';}
        var editor = CodeMirror.fromTextArea(document.getElementById(name), {
          mode: {name: mode, alignCDATA: true},
          lineNumbers: true,
          lineWrapping: false,
          onChange: updateEnvironment,
          lineNumbers: true,
          autofocus: false,
          matchBrackets: true,
          indentUnit: 4,
          indentWithTabs: true,
          enterMode: 'keep',
          tabMode: 'shift',
          autoClearEmptyLines: true,
          extraKeys: {
            'Ctrl-S': function(instance) { saveFile(instance); }
          },
          theme: 'ambiance'
        });
        setFullHeight();
        editor.refresh();
      }
      var createEditorInstance = function(fileName, filePath, isPreSetting) {
        if(isRunning && !isPreSetting) return;
        isRunning = true;
        setLoader(filePath);
        if(tabIsRendered(filePath))
        {
          activateTab(filePath, isPreSetting);
          removeLoader();
          isRunning = false;
          return;
        }
        var data =
        'action=get_file_content' +
        '&' +
        'file_path=' + filePath;
        $.ajax({
          data: data,
          success: function(response) {
            addNewTab(fileName, filePath, response, isPreSetting);
            if(!tabExists(filePath)) {
              addPreSettingsTabs(fileName, filePath);
            }
          }
        });
      }
      var isImage = function(fileName) {
        var isImage = false;
        var format = fileName.split('.')[1]
        if(format == 'jpg' || format == 'png' || format == 'gif' || format == 'bmp' || format == 'tif' || format == 'tiff') {
          isImage = true;
        }
        return isImage;
      }
      var addNewTab = function(fileName, filePath, response, isPreSetting) {
        var tabsContainer = $('#tabs');
        var editorContainer = $('#tabs-container');
        var tabIndex = tabsContainer.find('li').length+1;
        var tab = $('<li data-file="'+filePath+'"><a href="#editor-'+tabIndex+'">' + fileName + ' <span class="glyphicon glyphicon-remove"></span></a></li>');
        $(tabsContainer).append(tab);
        var content = '';
        if(!isImage(fileName)) {
          content = $('<div class="editor" id="editor-'+tabIndex+'"><textarea id="editor-code-'+tabIndex+'" name="css" rows="0" cols="0">'+response+'</textarea></div>');
          $(editorContainer).append(content);
          addEditor('editor-code-'+tabIndex, fileName);
        } else {
          content = $('<div class="editor image" id="editor-'+tabIndex+'"><img src="'+filePath+'"></div>');
          $(editorContainer).append(content);
        }
        try
        {
          editorContainer.tabs('destroy');
        }
        catch(err)
        {
          //console.log('error');
        }
        editorContainer.tabs();
        //editorContainer.tabs('load', tabIndex-1);
        if(!isPreSetting) {
          editorContainer.tabs('option', 'active', tabIndex-1);
          setSelectedPreSettingsTabs(tabIndex-1);
        } else {
          if($('#tabs li').length >= parseInt(presettings.activeTab)) {
            editorContainer.tabs('option', 'active', parseInt(presettings.activeTab));
          }
        }
        //editorContainer.tabs('load', 'editor-'+tabIndex);
        setEventsOnTabs();
        isRunning = false;
        removeLoader();
      }
      var removeTab = function(filePath) {
        var editorContainer = $('#tabs-container');
        var tab = $('#tabs').find('li[data-file="'+filePath+'"]');
        var editorContent = editorContainer.find(tab.find('a').attr('href'));
        tab.find('a').attr('href', 'javascript:void(0);');
        tab.remove();
        editorContent.remove();
        try
        {
          editorContainer.tabs('destroy');
        }
        catch(err)
        {
          //console.log('error');
        }
        editorContainer.tabs();
        removePreSettingsTabs(filePath);
        var count = $('#tabs li').length;
        if(count)
        {
          //editorContainer.tabs('option', 'active', 0);
        }
      }
      var setEventsOnTabs = function() {
        $('.nav .glyphicon-remove').unbind();
        $('.nav .glyphicon-remove').on('click', function() {
          //console.log('removeTab');
          removeTab($(this).parent().parent().data('file'));
        });
        $('.nav li').unbind();
        $('.nav li').on('click', function() {
          //console.log('setSelectedPreSettingsTabs');
          var index = $('#tabs').find('.ui-state-active').index();
          setSelectedPreSettingsTabs(index);
        });
      }
      //Presettings
      var getPreSettings = function() {
        if(presettings == null)
        {
          presettings =
          {
            expanded: [],
            tabs: [],
            activeTab: 0,
            selectedTreeItem: '/'
          };
        }
        return presettings;
      }
      var addPreSettingsExpanded = function(filePath) {
        var presettings = getPreSettings();
        presettings.expanded[presettings.expanded.length] = filePath;
        updatePreSettings(presettings);
      }
      var removePreSettingsExpanded = function(filePath) {
        var presettings = getPreSettings();
        if(filePath == '/') {
          presettings.expanded = [];
          presettings.selectedTreeItem = '/';
        } else {
          presettings.expanded = $.grep(presettings.expanded, function(n, i) {
            return (n != filePath);
          });
        }
        updatePreSettings(presettings);
      }
      var addPreSettingsTabs = function(fileName, filePath) {
        var presettings = getPreSettings();
        presettings.tabs[presettings.tabs.length] = {'fileName': fileName, 'filePath': filePath};
        updatePreSettings(presettings);
      }
      var setSelectedPreSettingsTabs = function(index) {
        var presettings = getPreSettings();
        presettings.activeTab = index;
        updatePreSettings(presettings);
      }
      var setSelectedPreSettingsTree = function(filePath) {
        var presettings = getPreSettings();
        //if(filePath.split('/').length <= 4)
        //{
          presettings.selectedTreeItem = filePath;
          updatePreSettings(presettings);
        //}
      }
      var removePreSettingsTabs = function(filePath) {
        var presettings = getPreSettings();
        presettings.tabs = $.grep(presettings.tabs, function(n, i) {
          return (n.filePath != filePath);
        });
        updatePreSettings(presettings);
      }
      var updatePreSettingsTabs = function(oldFilePath, newFilePath) {
        var presettings = getPreSettings();
        for(var i = 0;i<presettings.tabs.length;i++)
        {
          if(presettings.tabs[i].filePath == oldFilePath)
          {
            presettings.tabs[i].filePath = newFilePath;
            presettings.tabs[i].fileName = newFilePath.split('/')[newFilePath.split('/').length-1];
            var item = $('#tabs li[data-file="'+oldFilePath+'"]');
            item.find('> a').html(presettings.tabs[i].fileName + '<span class="glyphicon glyphicon-remove"></span>');
            item.attr('data-file', newFilePath);
          }
          break;
        }
        updatePreSettings(presettings);
      }
      var clearPreSettings = function() {
        $.removeCookie(cookieName, { expires: 7, path: '/' });
      }
      var updatePreSettings = function(presettings) {
        $.cookie(cookieName, JSON.stringify(presettings), { expires: 7, path: '/' });
      }
      var initPresettings = function() {
        if(presettings == null) {return;}
        //alert(JSON.stringify(presettings));
        //for(var i = 0;i<presettings.expanded.length;i++)
        //{
          //if(presettings.expanded[i].split('/').length <= 3) 
          //{
          if(presettings.expanded.length > 0) 
          {
            $('#filetree').jaofiletree('open', presettings.expanded[0], null, function() {
              $('#filetree').jaofiletree('setselected', presettings.selectedTreeItem);
              
              if(presettings.expanded.length > 1) {
                $('#filetree').jaofiletree('open', presettings.expanded[1], null, function() {
                  $('#filetree').jaofiletree('setselected', presettings.selectedTreeItem);
                  if(presettings.expanded.length > 2) {
                    $('#filetree').jaofiletree('open', presettings.expanded[2], null, function() {
                      $('#filetree').jaofiletree('setselected', presettings.selectedTreeItem);
                    });
                  }
                });
              }
            });
          }
          //}
        //}
        for(var i = 0;i<presettings.tabs.length;i++)
        {
          createEditorInstance(presettings.tabs[i].fileName, presettings.tabs[i].filePath, true);
        }
        $('#filetree').jaofiletree('setselected', presettings.selectedTreeItem);
      }
      //Filetree
      var initFileTree = function(connector) {
        $('#filetree').jaofiletree({
          script  : connector,
          showroot: root,
          expandSpeed: 100,
          collapseSpeed: 100,
          onclick : function(elem, type, filePath) {
            //console.log('1');
            $('#ho-context-menu').removeClass('open');
            if(type != 'dir')
            {
              //setSelectedPreSettingsTree(filePath);
              filePath = cleanFilePath(filePath);
              var fileName = filePath;
              if(fileName.indexOf('/') != -1)
              {
                fileName = fileName.split('/')[fileName.split('/').length-1];
              }
              createEditorInstance(fileName, filePath);
            }
            else
            {
              //alert(JSON.stringify($('#filetree').jaofiletree('getselected')));
              if(isExpanded(filePath)) {
                removePreSettingsExpanded(filePath);
              } else {
                addPreSettingsExpanded(filePath);
              }
              var currentSelected = $('#filetree').jaofiletree('getselected');
              if(currentSelected.length && currentSelected[0].file != filePath && currentSelected[0].file.indexOf(filePath) != -1) {
                //console.log(currentSelected);
                $('#filetree').jaofiletree('setselected', filePath);
                setSelectedPreSettingsTree(filePath);
              }
              //setSelectedPreSettingsTree(filePath);
            }
          },
          onclickcaption : function(elem, type, filePath) {
            //console.log('2');
            //console.log('onclickcaption was triggered : )');
            $('#ho-context-menu').removeClass('open');
            setSelectedPreSettingsTree(filePath);
          },
          ondoubleclick : function(elem, type, filePath) {
            //console.log('3');
            $('#ho-context-menu').removeClass('open');
            if(type != 'dir')
            {
              setSelectedPreSettingsTree(filePath);
              filePath = cleanFilePath(filePath);
              var fileName = filePath;
              if(fileName.indexOf('/') != -1)
              {
                fileName = fileName.split('/')[fileName.split('/').length-1];
              }
              createEditorInstance(fileName, filePath);
            }
            else
            {
              //alert(JSON.stringify($('#filetree').jaofiletree('getselected')));
              if(isExpanded(filePath)) {
                removePreSettingsExpanded(filePath);
              } else {
                addPreSettingsExpanded(filePath);
              }
              setSelectedPreSettingsTree(filePath);
            }
          }
        });
        $('#filetree').bind('initialized', function(){initPresettings();});
//        $('#filetree').bind('afteropen', function() {
//          //alert(filePath)
//          //addPreSettingsExpanded(filePath);
//        });
//        $('#filetree').bind('afterclose', function() {
//          //removePreSettingsExpanded(filePath);
//        });
      }
      var isExpanded = function(filePath) {
        var isExpanded = false;
        var fileTree = $('#filetree');
        var currentItem = fileTree.find('a[data-file="'+filePath+'"]').parent();
        if(currentItem.attr('class').indexOf('expanded') != -1)
        {
          isExpanded = true;
        }
        return isExpanded;
      }
      var cleanFilePath = function(filePath) {
        filePath = filePath.substring(0, filePath.length-1);
        return filePath;
      }
      //Editor
      var saveFile = function(editor) {
        if(editorIsSaving){return;}
        editorIsSaving = true;
        var codeTextArea = editor.getTextArea();
        var loaderObject = $('#tabs a[href="#'+$(codeTextArea).parent().attr('id')+'"]').parent();
        loaderObject.addClass('loader');
        editor.save();
        var code = editor.getValue();
        var filePath = $('#tabs a[href="#'+$(codeTextArea).parent().attr('id')+'"]').parent().data('file');
        //console.log(filePath);
        //return;
        //cleanFilePath($('#filetree').jaofiletree('getselected')[0].file);
        var data = 
        'action=save_file' +
        '&' +
        'file_path=' + filePath +
        '&' +
        'file_content=' + encodeURIComponent(code);
        $.ajax({
          type: 'POST',
          data: data,
          success: function(response) {
            //if(filePath.indexOf('.scss') != -1 || filePath.indexOf('options.json') != -1) {
            if(filePath.indexOf('.scss') != -1) {
              //clearTimeout(clearTimeoutPollSassError);
              //clearTimeoutPollSassError = setTimeout('pollSassError();', 5000);
              compileSass();
            } else if(filePath.indexOf('.less') != -1) {
              compileLess();
            } else {
              if(page) {
                page.location.reload();
              }
            }
            loaderObject.removeClass('loader');
            editorIsSaving = false;
            //if(filePath.indexOf('.less') != -1) {
            //  updateLessFilesInClient();
            //}
          }
        });
        //console.log('saveFile :' + code);
      }
      var compileSass = function() {
        $('#loader').slideDown('fast');
        $('#loader .text').html('Kompilerar SASS');
        var data = 
        'action=compile_sass' +
        '&' +
        'file_path=empty';
        $system.ajax({
          type: 'GET',
          data: data,
          success: function(response) {
            if(response.length > 0) {
              $('#alertModal').modal();
              $('#alertModal .modal-body').html(response);
              //$('#alertModal .btn').focus();
            } else {
              if(page) {
                page.location.reload();
              }
            }
            $('#loader').slideUp('fast');
          }
        });
      }
      var compileLess = function() {
        $('#loader').slideDown('fast');
        $('#loader .text').html('Kompilerar LESS');
        var data = 
        'action=compile_less' +
        '&' +
        'file_path=empty';
        $system.ajax({
          type: 'GET',
          data: data,
          success: function(response) {
            if(response.length > 0) {
              $('#alertModal').modal();
              $('#alertModal .modal-body').html(response);
              //$('#alertModal .btn').focus();
            } else {
              if(page) {
                page.location.reload(); 
              }
            }
            $('#loader').slideUp('fast');
          }
        });
      }
      var updateLessFilesInClient = function() {
        var lessCompilerIframe = $('#less-compiler');
        //alert(lessCompilerIframe.data('url'));
        lessCompilerIframe.load(function() {
          var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
          var eventer = window[eventMethod];
          var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

          // Listen to message from child window
          eventer(messageEvent, function(e) {
            var data = e.data;
            if(data.css_style.length > 0) {
              //alert(data.css_style.length);
              var data = 
              'action=update_less_files_from_client' +
              '&' +
              'file_path=empty' +
              '&' +
              'file_content=' + encodeURIComponent(data.css_style);
              $.ajax({
                type: 'POST',
                data: data,
                success: function(response) {
                  if(page) {
                    page.location.reload();
                  }
                }
              });
            }
          });
        });
        lessCompilerIframe.attr('src', lessCompilerIframe.data('src'));
      }
      var updateLessFiles = function() {
        var data = 
        'action=update_less_files' +
        '&' +
        'file_path=empty';
        $.ajax({
          type: 'POST',
          data: data,
          success: function(response) {
            if(page) {
              page.location.reload();
            }
          }
        });
      }
      //****************************************************************
      //Initialize
      //****************************************************************
      initFileTree(connector);
      if(pageAdmin.HemsidaOnline && pageAdmin.HemsidaOnline.UI.loader) {
        pageAdmin.HemsidaOnline.UI.loader.remove();
        pageAdmin.$system('html').css('overflow-y', 'hidden');
      }
      //setTimeout("$('#filetree').jaofiletree('setselected', '/');", 1000);
      //setTimeout("$('.filetree').focus();", 1200);
      //setTimeout("$('.filetree').select();", 1200);
      
      //initPresettings();
    });
  };
})(jQuery);
$system('body').ide();