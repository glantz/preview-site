var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser());

app.set('port', (process.env.PORT || 11000));

app.get('/', function(req, res) {
  if (req.query.url) {
    res.redirect(req.query.url);
  } else {
    res.send('You have to specify a url!!!');  
  }
  
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});